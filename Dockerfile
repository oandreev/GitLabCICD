FROM ubuntu

RUN apt-get update \
    && apt-get install wget gnupg make liblua5.3-dev git\
       libpcre++-dev libssl-dev zlib1g-dev libsystemd-dev -y \
    && wget -q -O - http://files.viva64.com/etc/pubkey.txt | \
       apt-key add - \
    && wget -O /etc/apt/sources.list.d/viva64.list \
       http://files.viva64.com/etc/viva64.list \
    && apt-get update \
    && apt-get install pvs-studio -y \
    && rm -rf /var/lib/apt/lists/*

